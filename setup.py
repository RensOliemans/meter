"""
Copyright 2020, Rens Oliemans.
Meter application
"""


import os

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'meter', '__version__.py'), 'r') as f:
    exec(f.read(), about)

with open('README.md', 'rb') as f:
    readme = f.read().decode("utf-8")

packages = ['meter']
install_requires = [
    'tkcalendar',
]

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    long_description_content_type='text/markdown',
    classifiers=[
        'Development Status :: 3 - BETA',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8'
    ],
    url=about['__url__'],
    author=about['__author__'],
    author_email=about['__author_email__'],
    license=about['__license__'],
    packages=packages,
    install_requires=install_requires,
    entry_points = {
        'console_scripts': [
            'meter = meter.__main__:main',
        ]
    },
    scripts=[
        'scripts/start.sh'
    ],
    python_requires='>=3.7',
    download_url="https://gitlab.com/RensOliemans/meter/-/archive/master/meter-master.tar.gz",
    project_urls={
        "Source": "https://gitlab.com/RensOliemans/meter",
        "Tracker": "https://gitlab.com/RensOliemans/meter/-/issues",
        # "Documentation": "https://gitlab.com/RensOliemans/meter/-/wikis/home",
    },
)
