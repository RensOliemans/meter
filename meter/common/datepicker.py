from datetime import date

import tkinter as tk
import tkcalendar as tkc


def get_cal(canvas):
    """Gets a calendar and a corresponding date variable"""
    date_variable = tk.StringVar(canvas)
    date_variable.set(str(date.strftime(date.today(), '%d/%m/%y')))
    calendar = tkc.Calendar(canvas, textvariable=date_variable,
                            date_pattern='dd/mm/yy')
    return calendar, date_variable
