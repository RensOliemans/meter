import tkinter as tk


def get_inputs(canvas, amount, callback=lambda: None):
    for i in range(amount):
        label = tk.Entry(canvas)
        label.bind('<Return>', func=lambda _: callback())
        yield label
