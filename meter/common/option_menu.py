import tkinter as tk


def get_options(canvas, options):
    """
    Gets an OptionMenu and a corresponding date variable.
    Default value of the OptionMenu is the first entry of the options list
    """
    if not options:
        raise ValueError("Options has to have at least 1 option!")
    option_variable = tk.StringVar(canvas)
    option_variable.set(options[0])
    option_menu = tk.OptionMenu(canvas, option_variable,
                                *options)
    return option_menu, option_variable
