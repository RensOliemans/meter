class Programme:
    def __init__(self, id, name):
        self.id = id
        self.name = name

    @staticmethod
    def items():
        return ['Name']


class Measurement:
    def __init__(self, programme, date, begin, end=None):
        self.programme = programme
        self.date = date
        self.begin = begin
        self.end = end

    @property
    def kwh(self):
        return self.end - self.begin

    def __repr__(self):
        return f"Measurement of programme {str(self.programme)}.\n" \
               f"Date: {self.date}, kWh: {self.kwh} ({self.end}-{self.begin})."


class MeasurementOfFilled(Measurement):
    def __init__(self, programme, date, begin, end=None, vol=1):
        super().__init__(programme, date, begin, end)
        self.vol = vol
        if not 0.0 <= vol <= 1.0:
            raise ValueError('Vol must be between 0 and 1')
        if end < begin:
            raise ValueError("End must not be lower than start")

    def __repr__(self):
        return f"Measurement of programme {str(self.programme)}.\n" \
               f"Date: {self.date}, kWh: {self.kwh} ({self.end}-{self.begin})," \
               f"{int(self.vol * 100)}% filled."
