import tkinter as tk


def place_results_on_grid(canvas, labels, results):
    for i, name in enumerate(labels):
        label = tk.Label(canvas, text=name, font=16)
        label.grid(row=0, column=i)

    for row, result in enumerate(results):
        for col, entry in enumerate(result):
            label = tk.Label(canvas, text=entry)
            label.grid(row=row + 1, column=col)
            yield label
