import datetime
import tkinter as tk
from abc import ABC, abstractmethod

from meter.common.data import Programme
from meter.common.datepicker import get_cal
from meter.common.inputs import get_inputs
from meter.common.option_menu import get_options
from meter.common.results import place_results_on_grid
from meter.general.application import Child


class InputResultsChild(Child, ABC):
    def __init__(self, master=None, cnf=None, **kw):
        super().__init__(master, cnf, **kw)
        self.used_labels = []

    def setup(self):
        self._setup_canvases()

        self._setup_programmes()
        self._setup_calendar()
        self.setup_input()

        self._setup_refresh_button()
        self.update()

        self._setup_submit_button()

    @property
    @abstractmethod
    def labels(self):
        pass

    @property
    @abstractmethod
    def programmes(self):
        pass

    @abstractmethod
    def setup_input(self):
        pass

    @abstractmethod
    def get_averages(self):
        pass

    @abstractmethod
    def get_inputs(self):
        pass

    @abstractmethod
    def insert_entry(self, *args):
        pass

    @abstractmethod
    def get_amount_measurements(self):
        pass

    def update(self):
        for label in self.used_labels:
            label.destroy()

        averages = sorted(self.get_averages())
        labels = self.labels
        self.used_labels = list(place_results_on_grid(self.results_canvas, labels, averages))

    def submit(self):
        self._setup_error()
        try:
            variables = self.get_inputs()
            self.insert_entry(*variables)
            self._set_success()
            self.update()
        except ValueError as e:
            self._set_error(e)

    def _set_success(self):
        self._ev.set('')
        self._sv.set(f'Gelukt, dit was de {self.get_amount_measurements()}e meting!')
        self._success.place(relx=.5, rely=.5)

    def _set_error(self, e):
        self._ev.set(f'Error {e}')
        self._error.place(relx=0.5, rely=.5)

    def _setup_canvases(self):
        self.input_canvas = tk.Canvas(self.parent, height=300, width=1200)
        self.input_canvas.pack()

        self.results_canvas = tk.Canvas(self.parent, height=300, width=1200)
        self.results_canvas.pack()

    def _setup_programmes(self):
        option_menu, self._pv = get_options(self.input_canvas, self.programmes)
        option_menu.place(relx=0.05, rely=0.15)

    def _setup_calendar(self):
        calendar, self._dv = get_cal(self.input_canvas)
        calendar.place(relx=0.25, rely=0.15)

    def _setup_refresh_button(self):
        tk.Button(self.input_canvas, text='Refresh', command=self.update).place(relx=.8, rely=.8)

    def _setup_submit_button(self):
        button = tk.Button(self.input_canvas, text='Submit', command=self.submit)
        button.place(relx=.8, rely=.12)

    def _setup_error(self):
        self._ev = tk.StringVar(self.input_canvas)
        self._sv = tk.StringVar(self.input_canvas)

        self._error = tk.Label(self.input_canvas, textvar=self._ev, bg="red")
        self._success = tk.Label(self.input_canvas, textvar=self._sv, bg="#51d0de")


class InputResultsVolumeChild(InputResultsChild, ABC):
    @staticmethod
    @abstractmethod
    def convert_programme(pv):
        pass

    def get_inputs(self):
        programme = self.convert_programme(self._pv.get())
        date = datetime.datetime.strptime(self._dv.get(), '%d/%m/%y')
        b = float(self._begin.get())
        e = float(self._end.get())
        v = float(self._vol.get())
        return programme, date, b, e, v

    def setup_input(self):
        self._setup_labels()
        self._setup_inputs()

    def _setup_labels(self):
        tk.Label(self.input_canvas, text="Programma (naam,temp,rpm)").place(relx=0.05, rely=0.05)
        tk.Label(self.input_canvas, text="Datum").place(relx=0.25, rely=0.05)
        tk.Label(self.input_canvas, text="Begin (kWh)").place(relx=0.50, rely=0.05)
        tk.Label(self.input_canvas, text="Eind (kWh)").place(relx=0.60, rely=0.05)
        tk.Label(self.input_canvas, text="Vol (0-1)").place(relx=0.70, rely=0.05)

    def _setup_inputs(self):
        self._begin, self._end, self._vol = list(get_inputs(self.input_canvas, 3, self.submit))

        self._begin.place(relx=0.50, rely=0.15, width=100)
        self._end.place(relx=0.60, rely=0.15, width=100)
        self._vol.place(relx=0.70, rely=0.15, width=100)
