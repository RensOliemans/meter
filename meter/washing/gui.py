from meter.common.children import InputResultsVolumeChild
from meter.washing import sql
from meter.washing.data import WashingMeasurement


class WashingMachine(InputResultsVolumeChild):
    @property
    def labels(self):
        return ['Name', 'Temp', 'RPM', 'Average kWh', 'Measurements']

    @property
    def programmes(self):
        return list(sql.get_programmes())

    def get_averages(self):
        return list(sql.get_averages())

    def insert_entry(self, *args):
        w = WashingMeasurement(*args)
        sql.insert_measurement(w)

    def get_amount_measurements(self):
        return sql.get_amount_measurements()

    def setup_input(self):
        super().setup_input()
        self._begin.insert(0, sql.get_previous_end())

    @staticmethod
    def convert_programme(pv):
        pv = pv[1:-1]
        name, temp, rpm = pv.split(', ')
        return sql.get_programme(name, temp, rpm)
