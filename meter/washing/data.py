from meter.common.data import Programme, MeasurementOfFilled


class WashingProgramme(Programme):
    def __init__(self, id, name, temperature, rpm):
        super().__init__(id, name)
        self.temperature = temperature
        self.rpm = rpm

    @staticmethod
    def items():
        return ['Name', 'Temp', 'Average kWh', 'RPM' 'Measurements']

    def __str__(self):
        return f"({self.name}, {self.temperature}, {self.rpm})"

    def __repr__(self):
        return f"Programme: {self.id}, {self.name}, {self.temperature}, {self.rpm}"


class WashingMeasurement(MeasurementOfFilled):
    pass
