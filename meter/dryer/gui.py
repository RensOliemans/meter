from meter.common.children import InputResultsVolumeChild
from meter.dryer import sql
from meter.dryer.data import DryerMeasurement


class Dryer(InputResultsVolumeChild):
    @property
    def labels(self):
        return ['Name', 'Average kWh', 'Measurements']

    @property
    def programmes(self):
        return list(sql.get_programmes())

    def get_averages(self):
        return list(sql.get_averages())

    def insert_entry(self, *args):
        w = DryerMeasurement(*args)
        sql.insert_measurement(w)

    def get_amount_measurements(self):
        return sql.get_amount_measurements()

    @staticmethod
    def convert_programme(pv):
        pv = pv[1:-1]
        name, fabric = pv.split(', ')
        return sql.get_programme(name, fabric)
