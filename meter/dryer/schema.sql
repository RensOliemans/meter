CREATE TABLE IF NOT EXISTS "programme"
(
    id int
          primary key,
    name text not null unique
);
CREATE TABLE IF NOT EXISTS "measurement"
(
    id int
      primary key,
    date text not null,
    vol real default 1,
    begin real not null,
    end real,
    pid int
         references programme
    check (vol <= 1.0)
    check (vol >= 0.0)
);
