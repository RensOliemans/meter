import sqlite3

from meter.dryer.data import DryerProgramme
from meter.general.util import get_root_location

db_location = f"{get_root_location()}/dryer/dryer.db"
conn = sqlite3.connect(db_location)
c = conn.cursor()


def get_programmes():
    c.execute('SELECT id,name,fabric FROM programme ORDER BY fabric,name DESC')
    return (DryerProgramme(p[0], p[1], p[2]) for p in c.fetchall())


def get_averages():
    c.execute('SELECT programme.name, programme.fabric, '
              'round(avg(measurement.end - measurement.begin),3),'
              'count(measurement.pid) '
              'FROM programme, measurement '
              'WHERE programme.id = measurement.pid '
              'GROUP BY programme.id')
    return c.fetchall()


def insert_measurement(measurement):
    c.execute('SELECT max(id) FROM measurement')
    max_id = c.fetchone()[0] or 0
    c.execute('INSERT INTO measurement (id, date, vol, begin, end, pid) VALUES'
              '(?, ?, ?, ?, ?, ?)',
              (max_id + 1, str(measurement.date), measurement.vol, measurement.begin,
               measurement.end, measurement.programme.id))
    conn.commit()


def get_amount_measurements():
    c.execute('SELECT count(*) FROM measurement')
    return c.fetchone()[0]


def get_programme(name, fabric):
    c.execute('SELECT id,name,fabric FROM programme WHERE name=? '
              'AND fabric=?',
              (name, fabric))
    results = c.fetchone()
    return DryerProgramme(results[0], results[1], results[2])
