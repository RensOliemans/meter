from meter.common.data import Programme, MeasurementOfFilled


class DryerProgramme(Programme):
    def __init__(self, id, name, fabric):
        super().__init__(id, name)
        self.fabric = fabric

    def __str__(self):
        return f"({self.name}, {self.fabric})"


class DryerMeasurement(MeasurementOfFilled):
    pass
