import tkinter as tk

from meter.dryer.gui import Dryer
from meter.general.application import Application
from meter.washing.gui import WashingMachine


def create_root(children):
    root = tk.Tk()
    app = Application(root)
    app.pack(side='top', fill='both', expand=True)
    for child in children:
        app.add(child)
    return root


def main():
    w = WashingMachine()
    d = Dryer()
    root = create_root([w, d])
    root.mainloop()
