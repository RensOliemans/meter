import os


def get_root_location():
    here = os.path.dirname(__file__)
    root = os.path.join(here, os.pardir)
    return os.path.abspath(root)
