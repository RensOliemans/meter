import sys
from abc import ABC, abstractmethod

import tkinter as tk


class Application(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self._set_title()
        self._setup_close()

    def add(self, child):
        child.register(self)

    def _set_title(self):
        self.parent.title("Stroommeter")

    def _setup_close(self):
        def close(_):
            self.parent.withdraw()
            sys.exit()

        self.parent.bind('<Escape>', close)


class Child(ABC):
    def __init__(self, master=None, cnf=None, **kw):
        self.master = master
        self.kw = kw
        self.cnf = cnf if cnf is not None else {}
        self.canvas = None
        self.parent = None

    def register(self, parent):
        self.parent = parent
        self.setup()

    @abstractmethod
    def setup(self):
        pass
